# Accelerating Speed-to-Market

With an end goal of increasing quality and reducing the amount of time required for testing, Shifting Left ensures that both of these are met. By waiting to evaluate later in the development lifecycle, the cost of fixing any security concerns significantly increases.

![Shift Left Cost Model](Images/Shift_Left_Cost_Model.png){width=50%}

# Improving Security Protections

Historically, development teams may have been reluctant to implement or engage in a Shift Left approach because they believed involving security too early in the process may delay or complicate a project. However, DevOps and the associated tooling landscape has changed in recent years, and Shifting Left has become increasingly practical, and a best practice in the worlds of both DevOps and security.

![Lifecycle Tooling](Images/Lifecycle_Tooling.png){width=50%}

# Implementing Shifting Left

Shifting Left begins with establishing collaboration between the entire security and DevOps teams. Integrating the importance of security into the workforce culture purports responsibly among all individuals within an organisation. Ensuring this buy-in is vital for success. For example, developers must be bought into the Shift Left approach when they code with security top of mind.

![DevSecOps Feedback Loop](Images/DevSecOps_Feedback_Loop.png){width=50%}

CISO and security leaders should encourage their teams to engage in regular conversations about application security via the development process. Security must continue to speak with their developer counterparts so Shifting Left becomes embedded in their process.

Testing is also key in ensuring shifting left takes place. Testing automation and continuous integration are vital components and things to be mindful of, especially as developers are becoming comfortable with Shifting Left.
