# Adoption of an Industry Framework

- [National Institute of Standards and Technology](https://www.nist.gov/) (NIST) [Framework for Improving Critical Infrastructure Cybersecurity](https://www.nist.gov/cyberframework) (NIST CSF) 

  Used by 29% of organisations, NIST is a voluntary framework intended primarily for critical infrastructure organisations to manage and mitigate cybersecurity risk based on existing standards, guidelines, and practices. 
  That being said, the Cybersecurity Framework has proven to be flexible enough to also be applied by non-US and non-critical infrastructure organisations.

- [Center for Internet Security Critical Security Controls](https://www.cisecurity.org/controls/cis-controls-list/) (CIS) 
  Used by 32% of organisations, the CIS Critical Security Controls are a set of 20 actions designed to mitigate the threat of common cyber attacks.
  The controls were designed by a group of volunteer experts from a range of fields, including cyber analysts, academics, auditors, and consultants.

- [ISO 27001/27002](https://www.iso.org/isoiec-27001-information-security.html)
  Used by 35% of organisations, ISO 27001 is the international standard that describes best practice for implementing an ISMS (information security management system).
  Achieving accredited certification to ISO 27001 shows that your company is following information security best practice, and delivers an agnostic expert assessment of whether your data is adequately protected.

# Focus on The Basics
1. __[Application Security](https://www.vmware.com/topics/glossary/content/application-security)__
is a term that describes security measures at the application level that aim to protect critical data from external threats by ensuring the security of all of the software used to run the business. This area of security helps identify, fix and prevent security vulnerabilities in any kind of software application. Types of Application Security activities include: authentication, authorization, encryption, and logging. 

2. __[Security Assessments](https://danielmiessler.com/study/security-assessment-types/)__
and audits are crucial to the success of a security program. These studies work to explicitly locate IT vulnerabilities and risks within an organisation’s system and infrastructure. They identify vulnerabilities, such as faulty firewall, lack of system updates, malware, or other risks that can impact their function and performance.
Types of Assessments include: Penetration Testing, Vulnerability Assessments, Audits, Risk Assessments, Threat Assessments, Red Team Assessments, White/Grey/Black-Box Assessments, and more 

3. __[Cloud Security](https://www.mcafee.com/enterprise/en-us/security-awareness/cloud.html)__
involves the procedures and technology that secure cloud computing environments against both external and insider cybersecurity threats. Based on your deployment model, you can classify your cloud as public, private, or hybrid (a combination of both public and private).  
There are three different types of cloud computing: Software as a Service (SaaS), Platform as a Service (PaaS), and Infrastructure as a Service (IaaS). The latter is the most common cloud service, offering data storage and virtual servers.  

4. __[Cyber Threat Intelligence](https://www.gartner.com/en/documents/2487216/definition-threat-intelligence)__
is often broken up into three subcategories:
    
    __Strategic:__ intelligence explains threats for a non-technical audience (stakeholders include C-level executives and board members) 
    __Tactical:__ intelligence describes threat conditions for technical audiences (stakeholders include SOC analysts, SIEM, and Endpoints) 
    __Operational:__ intelligence details hacker information and intent (stakeholders include Threat Hunters, SOC Analysts, and employees in charge of Incident Response) 

5. __[Email Security](https://www.techopedia.com/definition/29704/email-security)__
refers to the collective measures used to secure the access and content of an email account or service. It allows an individual or organisation to protect the overall access to their email addresses and accounts. From an individual/end user standpoint, proactive email security measures include strong passwords, password rotations, spam filters, and desktop-based anti-virus applications. 

6. __[Endpoint Security](https://www.mcafee.com/enterprise/en-us/security-awareness/endpoint.html)__
systems defend and protect computers and other devices from cybersecurity threats, whether that be on your network or in the cloud. An endpoint refers to any remote computing device that communicates back and forth with a network. Examples include smartphones, desktops, tablets, laptops, and servers.    

7. __[GRC (Governance, Risk, and Compliance)](https://www.oceg.org/about/what-is-grc/)__
is often described as a collection of capabilities that enable an organisation to reliably achieve their objectives, act with integrity, and address uncertainties. This relatively new [corporate management system](https://www.investopedia.com/terms/g/grc.asp) integrates governance, risk, and compliance into the processes of every department within an organisation. This system was intended to correct the "silo mentality" that leads departments inside of an organisation to hoard information and/or resources.  

8. __[Identity and Access Management (IAM)](https://www.csoonline.com/article/2120384/what-is-iam-identity-and-access-management-explained.html)__
is the process of defining and managing the roles and access privileges of individual network users and the circumstances in which users are granted (or denied) those privileges. Those users could be either customers or employees. Identity management systems are available for both on-premises and cloud-based systems. 

9. __[Internet of Things (IoT)](https://internetofthingsagenda.techtarget.com/definition/IoT-security-Internet-of-Things-security)__
involves adding internet connectivity to a system of interrelated computing devices, mechanical and digital machines, objects, animals and/or people. Each "thing" is provided a unique identifier and the ability to automatically transfer data over a network. Allowing devices to connect to the internet opens them up to a number of serious vulnerabilities if they are not properly protected. 

10. __[Security Awareness and Training](https://resources.infosecinstitute.com/category/enterprise/securityawareness/#gref)__
is a formally documented process used to educate employees on computer security and IT protection. The goal of a security awareness program is to increase both organisational understanding and practical implementation of security best practices. The programs themselves are important, but it's just as important for employees to be held responsible and that steps are taken to gauge how effective your organisation's processes are.

# Innovative Approach to Challenges

While every organisation and vertical has its own specific set of challenges, security professionals generally agree on the top challenges they face while running a strategic security program. Here are the top 5 common challenges that security faces:

Budget [11 Trends to Inform Your 2020 Cybersecurity Budget, CSO Online](https://securityintelligence.com/articles/11-stats-on-ciso-spending-to-inform-your-2020-cybersecurity-budget/)

Hiring and Retention [Infographic, The Cybersecurity Skills Gap](https://www.klogixsecurity.com/blog/infographic-the-cybersecurity-skills-gap)

Business Alignment [Management Guide for CISOs](https://resources.infosecinstitute.com/management-guide-for-cisos-responsibilities-strategies-and-best-practices/#gref)

Reducing Complexity [Read more in September 2019 issue of Feats of Strength](https://www.klogixsecurity.com/feats-of-strength-sept-2019)

Metrics/Showing progress [When Security Metrics Miss the Point](https://www.klogixsecurity.com/blog/when-security-metrics-miss-the-point) 

# Awareness of Industry Trends

__Artificial Intelligence__


CISOs believe AI is moving beyond a buzzword. Many are actively researching and investing in AI technologies they believe will help their security programs. According to the trends we collected in our March issue of Feats of Strength, AI is the third most popular area in which CISOs are investing in this year.

__Automation and Orchestration__


CISOs are looking to invest in automation technologies that work to free up time and resources for their security people, so they can instead focus on strategic and high-value opportunities. As an example, information security is moving into other parts of information security, namely the DevSecOps space.

__Clearing the Clutter__


There's no denying that the marketspace is saturated with security products and vendors, but how do you filter through the clutter and decide on the solutions that are best for your organisation? We asked leading CISOs to weigh in on this topic.

__Cloud Migration__


According to our profile with Michael Charland, Global ISO at Hartford Steam Boiler, he believes "although many organisations have already begun moving to the cloud, they often have not taken time to provide training to their IT and/or security teams on the differences of how to manage security for cloud. There are many changes in how we manage security in the cloud based on whether the solution is SaaS, IaaS, or PaaS. When moving to cloud, we need to make sure that compliance is in place for our cloud configurations. Automation must be used as much as possible and...we need to understand and automate processes with policy and automation in place prior to moving to new technologies."

__Privacy Regulations__


When asked about the top challenges that force CISOs to redirect their time and focus away from strategic tasks, privacy regulations were among the top. These shifts and developments in regulations constantly redirect security team's time. With regulations like GDPR and CCPA to focus on, CISOs are forced to spend more time interacting with their legal and compliance teams.

__Security Culture/Awareness__


Behind a focus on cloud, increasing security awareness is the top goal for CISOs this year. They agree that in order to do this, they have to spend budget on building a stronger security culture within their organisations. CISOs are becoming increasingly more innovative with their attempts to embed security into the fabric of their companies.

__Shift Left Security__


Shift Left was mentioned by 54% of the CISOs we spoke with at the RSA Conference this year. They discussed the term in the context of improving quality by moving tasks to the ‘left’ as early in the technology lifecycle as possible, so you spend less time, energy, and resources on dealing with security issues. 

__Zero Trust Models__


Much like AI, Zero Trust Models are moving beyond a buzzword. In the Zero Trust model, no one and nothing is granted access until it has been verified. Zero Trust makes up for the dangers created by interconnection in the cloud, and as highly-connected cloud-based environments and threat landscapes continue to emerge and expand, the need for Zero Trust-based strategies is apparent now more than ever.

# Continuous Personal Development  

In order to be strategic, make security a competitive advantage, and align with the business, it's crucial to make sure you're staying on top of the latest communication channels. Here are a few ways to stay current: 

__Read up on Industry Publications__
- [Dark Reading](https://www.darkreading.com/)
- [CSO Online](https://www.csoonline.com/)
- [InfoSecurity Magazine](https://www.infosecurity-magazine.com/)
- [Krebs on Security](https://krebsonsecurity.com/)

__Attend Notable Industry Events__

__Utilise Information from Research Firms like__ [ISACA](https://www.isaca.org/)

__Stay Active and Engaged with Top Social Platforms:__
- LinkedIn
- Twitter 
- Podcasts like [Security Now!](https://www.grc.com/securitynow.htm), [Security Weekly](https://securityweekly.com/), and [Risky Business](https://risky.biz/netcasts/risky-business/)

At the end of the day, security isn't just an issue for the Security Department, it's a priority for the organisation as a whole. In an attempt to create a culture of shared cyber risk ownership, your program must be strategic and business-focused. By focusing on the five components listed above, you set your organisation up for success and create a foundational backbone for your security program.  
